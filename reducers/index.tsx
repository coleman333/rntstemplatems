import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

// import userReducer from './userReducer';
import common from './common';
// const common = require('./common');

export default combineReducers({
    router: routerReducer,
    // userReducer: userReducer,
    common: common as any
});
