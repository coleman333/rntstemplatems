import _ from 'lodash'
// import * as simpleActions from '../actions/actions';
// declare module simpleActions
import {createReducer} from 'redux-act';
const simpleActions = require('../actions/actions');

const initialState = {
    amountOfActiveProcesses: 0,
};

const common = createReducer({

    [ simpleActions.addProcess ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.amountOfActiveProcesses++;
        return newState
    },

    [ simpleActions.removeProcess ]: (state, payload) => {
        const newState = _.cloneDeep(state);
        newState.amountOfActiveProcesses--;
        return newState
    },

}, initialState);

export default common;
