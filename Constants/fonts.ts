type FontStyle = 'normal' | 'italic';
type FontType =
  | 'thin'
  | 'extra-light'
  | 'light'
  | 'regular'
  | 'medium'
  | 'semi-bold'
  | 'bold'
  | 'extra-bold'
  | 'black';

const FontFamilyMap: { [type: string]: string } = {
  /* 100 */ 'thin-normal': 'Montserrat-Thin',
  /* 100 */ 'thin-italic': 'Montserrat-ThinItalic',
  /* 200 */ 'extra-light-normal': 'Montserrat-ExtraLight',
  /* 200 */ 'extra-light-italic': 'Montserrat-ExtraLightItalic',
  /* 300 */ 'light-normal': 'Montserrat-Light',
  /* 300 */ 'light-italic': 'Montserrat-LightItalic',
  /* 400 */ 'regular-normal': 'Montserrat-Regular',
  /* 400 */ 'regular-italic': 'Montserrat-RegularItalic',
  /* 500 */ 'medium-normal': 'Montserrat-Medium',
  /* 500 */ 'medium-italic': 'Montserrat-MediumItalic',
  /* 600 */ 'semi-bold-normal': 'Montserrat-SemiBold',
  /* 600 */ 'semi-bold-italic': 'Montserrat-SemiBoldItalic',
  /* 700 */ 'bold-normal': 'Montserrat-Bold',
  /* 700 */ 'bold-italic': 'Montserrat-BoldItalic',
  /* 800 */ 'extra-bold-normal': 'Montserrat-ExtraBold',
  /* 800 */ 'extra-bold-italic': 'Montserrat-ExtraBoldItalic',
  /* 900 */ 'black-normal': 'Montserrat-Black',
  /* 900 */ 'black-italic': 'Montserrat-BlackItalic',
};

export const getFontFamily = (
  type: FontType = 'regular',
  fontStyle: FontStyle = 'normal',
): string => FontFamilyMap[`${type}-${fontStyle}`];
