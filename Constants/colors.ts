import { COLOR } from 'react-native-material-ui';

export const COLORS = {
    // Material-UI colors
    ...COLOR,

    // Custom colors
    navy: '#00253c',
    lightNavy: '#416c91',
    OMNIAZ_submenu: '#314d68',
    inactive: '#7597b7',
    mauve: '#b1687d',
    lightMauve: '#e497ac',
    gray: '#dcddde',
    grayButton: 'rgba(187, 193, 198, 1)',
    grayOverlay: '#878787',
    grayBackground: '#e3e3e3',
    icon: '#303030',
    lightTextColor: 'rgba(0, 0, 0, 0.87)',
    lightBorderColor: 'rgba(0, 0, 0, 0.12)',
    placeholderColor: 'rgba(0, 0, 0, 0.6)',
};
