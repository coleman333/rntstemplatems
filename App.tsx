import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
// import FirstTSComponent from './Containers/First';
import { RootContainer } from './Containers/Root/RootContainer';

// const FirstTSComponent = require('./Containers/First').default;

// type Props = {};
// export default class App extends Component<Props> {
//   render() {
//     // const name = 'Yegor';
//
//     return (
//       <View style={styles.container}>
//         <FirstTSComponent name={'Yegor'} enthusiasmLevel={5} />
//
//       </View>
//     );
//   }
// }


type Props = {};
export default class App extends Component<Props> {
  render() {
     const name = 'Yegor';
    console.log("========",name,"++++++++++")
    return (
      <View style={styles.container}>
        <RootContainer  />

      </View>
    );
  }
}


// export default RootContainer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
