import { createStore , applyMiddleware , compose} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import _ from 'lodash';
// import createLogger from 'redux-logger';

import rootReducer from './reducers/index';
// import simpleActions from './actions/actions';
const simpleActions =require('./actions/actions');
// const rootReducer = require('./reducers/index');




const manageAmountOfActiveProcesses = (store: any) => (next:any) => (action:any) => {
    const { dispatch } = store;
    const result = next(action);
    const actionType = _.get(action, 'type');

    if ( _.endsWith(actionType, '_REQUEST') ) {
        dispatch(simpleActions.addProcess());
    }

    if ( _.endsWith(actionType, '_OK') || _.endsWith(actionType, '_ERROR') ) {
        dispatch(simpleActions.removeProcess());
    }

    return result;
};

const middleware = [ thunk, promise, manageAmountOfActiveProcesses ];

export default function configureStore(initialState: any){
    // const logger = createLogger();\
    console.log('configureStore=>>')
    const enhancer= compose(applyMiddleware(...middleware));
    return createStore(rootReducer, initialState, enhancer);
}
