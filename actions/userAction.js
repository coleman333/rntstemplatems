// import { createActionAsync } from "redux-act-async";
// import axios from "axios";
// // import { createAction } from "redux-actions";
// import _ from 'lodash';
// import { AsyncStorage } from "react-native"
//
// async function getServerConfigs() {
//     try {
//         const port = await AsyncStorage.getItem('port');
//         const address = await AsyncStorage.getItem('address');
//
//         return {port, address}
//     } catch (error) {
//         console.log(error)
//     }
// }
//
// export default {
//     getTestUsers: createActionAsync('GET_TEST_USERS', () => {
//        return axios({
//             method: 'get',
//             url: `https://jsonplaceholder.typicode.com/users`,
//         });
//     }, {rethrow: true}),
//
//     login: createActionAsync('LOGIN', async (user) => {
//         const {port,address}=await getServerConfigs();
//         return axios({
//             method: 'post',
//              url: `http://192.168.0.201:3003/api/users/login`,
//              // url: `http://192.168.0.201:4433/api/users/login`,
//             data:  {user},
//         });
//     }, {rethrow: true}),
//     productScan: createActionAsync('PRODUCT', (product) => {
//         return axios({
//             method: 'get',
//             url: `http://192.168.0.109:4433/api/users/product`,product
//             // data: { user},
//         });
//     }, {rethrow: true}),
//
//     getCodeResponse: createActionAsync('GET_CODE_RESPONSE', (code) => {
//         return axios({
//             method: 'post',
//             // url: `http://192.168.0.201:3009/api/users/get-code-response`,
//              url: `http://207.154.215.144:3009/api/users/get-code-response`,
//
//             data: { code},
//         });
//     }, {rethrow: true}),
//     getAllItems: createActionAsync('GET_ALL_ITEMS', () => {
//         return axios({
//             method: 'post',
//             // url: `http://192.168.0.201:3009/api/users/get-all-items`,
//              url: `http://207.154.215.144:3009/api/users/get-code-response`,
//
//             // data: { code},
//         });
//     }, {rethrow: true}),
//
//     readText: createActionAsync('READ_TEXT', async ({base64:base64content, uri}) => {
//         // alert(`some text here ${uri}`);
//         // console.log(base64content);
//         // let data = new FormData();
//         // data.append('file',base64content);
//         // const {port,address}=await getServerConfigs();
//
//         return axios({
//             method: 'post',
//             // url: `http://192.168.0.201:3009/api/users/test-tesseract-ocr`,
//              url: `http://207.154.215.144:3009/api/users/test-tesseract-ocr`,
//             // url: `http://192.168.137.2:3003/api/users/test-tesseract-ocr`,
//             data:{fileBase64content:base64content, extension:_.last(_.split(uri,'.'))}
//         });
//     }, {rethrow: true}),
//
//     saveToBaseEditedResult: createActionAsync('SAVE_TO_BASE_EDITED_RESULT', (confirmedText) => {
//         // alert(`some text confirmedText);
//         return axios({
//             method: 'post',
//             // url: `http://192.168.0.201:3009/api/users/save-to-base-edited-result`,
//             url: `http://207.154.215.144:3009/api/users/save-to-base-edited-result`,
//             data:{confirmedText},
//         });
//     }, {rethrow: true}),
//
//     engraving: createActionAsync('ENGRAVING', async (text) => {
//         // alert(`some text here ${uri}`);
//         // console.log(base64content);
//         // let data = new FormData();
//         // data.append('file',base64content);
//         const {port,address}=await getServerConfigs();
//
//         return axios({
//             method: 'post',
//             url: `http://${address}:${port}/api/users/test-tesseract-ocr`,
//             data:{text},
//         });
//     }, {rethrow: true}),
//
//     sendPushNotification: createActionAsync('SEND_PUSH_NOTIFICATION', async (message) => {
//         console.log(message , 'this is action side');
//         const {port,address}=await getServerConfigs();
//
//         return axios({
//             method: 'post',
//             url: `http://192.168.0.201:3003/api/users/send-push-notifications`,
//             data: { message},
//         });
//     }, {rethrow: true}),
//
// };
//
